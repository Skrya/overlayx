﻿using GameOverlay.Drawing;
using GameOverlay.Windows;
using System;

namespace OverlayX
{
    public class OverlayWindow 
    {
        private readonly GraphicsWindow _window;

        private Font GFXFont;
        private SolidBrush GFXBlack;
        private SolidBrush GFXGray;
        private SolidBrush GFXRed;
        private SolidBrush GFXGreen;
        private SolidBrush GFXBlue;
        private Image GFXImage;

        private int midX;
        private int midY;

        Functions functions = new Functions();

		  public bool isCrosshairEnabled = false;
		  public bool isMonitoringEnabled = false;
		  public bool isPlaceholderEnabled = false;

        public OverlayWindow()
        {
            // initialize a new Graphics object
            // GraphicsWindow will do the remaining initialization
            var graphics = new Graphics
            {
                MeasureFPS = true,
                PerPrimitiveAntiAliasing = true,
                TextAntiAliasing = true,
                UseMultiThreadedFactories = false,
                VSync = true,
                WindowHandle = IntPtr.Zero
            };

            // it is important to set the window to visible (and topmost) if you want to see it!
            _window = new GraphicsWindow(graphics)
            {
                IsTopmost = true,
                IsVisible = true,
                FPS = 10,
                X = 0,
                Y = 0,
                Width = functions.GetScreenWidth(),
                Height = functions.GetScreenHeight()
            };

            _window.SetupGraphics += _window_SetupGraphics;
            _window.DestroyGraphics += _window_DestroyGraphics;
            _window.DrawGraphics += _window_DrawGraphics;
        }

        ~OverlayWindow()
        {
            // you do not need to dispose the Graphics surface
            try
            {
                _window.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e);
                throw;
            }
        }

        public void Initialize() 
        {
            midX = _window.Width / 2;
            midY = _window.Height / 2;

				Console.WriteLine(Globals.screenWidth);
        }

        public void Run()
        {
            // creates the window and setups the graphics
            _window.StartThread();
        }

        public void Stop()
        {
            _window.Dispose();
            functions.EXIT();

        }

        private void _window_SetupGraphics(object sender, SetupGraphicsEventArgs e)
        {
            var gfx = e.Graphics;

            //FONT STYLE
            GFXFont = gfx.CreateFont("Arial", 12);

            //CREATE BRUSHES (input: 0-1 or 0-255)
            GFXBlack = gfx.CreateSolidBrush(0, 0, 0);
            GFXGray = gfx.CreateSolidBrush(0x24, 0x29, 0x2E, 0.8f);

            GFXRed = gfx.CreateSolidBrush(255,0,0,0.7f);
            GFXGreen = gfx.CreateSolidBrush(Color.Green);
            GFXBlue = gfx.CreateSolidBrush(Color.Blue);

				//GFXImage = gfx.CreateImage(Resources.image); // loads the image using our image.bytes file in our resources
				GFXImage = gfx.CreateImage("./Resources/crosshair-normal.png");
                       
        }

        private void _window_DrawGraphics(object sender, DrawGraphicsEventArgs e)
        {
            //Code here
            int[] mouseXY = functions.GetMousePosition(); // X Y

            float cpuUtilization = functions.GetCpuUtilization() * (158 / 100);
            float ramUtilization = functions.GetRamUtilization() * (158 / functions.GetAvailableMemory());

            // you do not need to call BeginScene() or EndScene()
            var gfx = e.Graphics;

            //BACKGROUND COLOR
            gfx.ClearScene(new Color(0, 0, 0, 0)); //gray - 0.5 opacity //Gray = Color(125,125,125,125)

            //DRAW COMPONENTS
            if ((mouseXY[0] <= 10 && (mouseXY[1] <= midY + 100 && mouseXY[1] >= midY - 100)) || Globals.isMonitoring) {
                gfx.FillRoundedRectangle(GFXGray, -70, midY + 100, 70, midY - 100, 5);

                gfx.DrawRectangle(GFXBlack, 10, midY + 70, 30, midY - 90, 2);
                gfx.DrawRectangle(GFXBlack, 40, midY + 70, 60, midY - 90, 2);

                gfx.FillRectangle(GFXRed, 11, midY + 69, 29, midY - 89 + (158 - cpuUtilization));
                gfx.FillRectangle(GFXRed, 41, midY + 69, 59, midY - 89 + (158 - ramUtilization));

                gfx.DrawText(GFXFont, GFXRed, 8, midY + 77, "CPU");
                gfx.DrawText(GFXFont, GFXRed, 38, midY + 77, "MEM");
            }
            /*else
            {
                gfx.FillRoundedRectangle(GFXGray, -130, midY + 100, 10, midY - 100, 5);
            }*/

				if(Globals.isCrosshair)
					 gfx.DrawImage(GFXImage, midX-35, midY- 35, midX+ 35, midY+ 35, 1);

        }

        private void _window_DestroyGraphics(object sender, DestroyGraphicsEventArgs e)
        {
            // you may want to dispose any brushes, fonts or images
        }
    }
}
