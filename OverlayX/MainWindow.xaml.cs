﻿using System;
using System.Windows;
using Forms = System.Windows.Forms;
using Controls = System.Windows.Controls;
using System.Drawing;

using System.Threading;

namespace OverlayX
{
	 public partial class MainWindow : Window
	 {
		  //Shared variables
		  Thread overlayMainThread;
		  private static bool programRunning = true;

		  public bool isMonitoring = true;
		  public bool isCrosshair = true;

		  public Controls.TextBlock sajt;

		  string[] consoleLines;
		  
		  #region SETTINGS WINDOW
		  public MainWindow()
		  {
				InitializeComponent();
				//Set window parameters
				ResizeMode = ResizeMode.CanMinimize;
				
				//Start overlay window
				overlayMainThread = new Thread(OverlayMain);
				overlayMainThread.Start();

				sajt = windowConsole;

				consoleLines = new string[8];
				for (int i = 0; i < 8; i++)
				{
					 consoleLines[i] = "";
				}
				UpdateConsole("Console test!\n");
				/*UpdateConsole("Console ng...\n");
				UpdateConsole("Console running...\n");*/
		  }

		  public void UpdateConsole(string text)
		  {
				sajt.Inlines.Clear();
				for (int i = 0; i < 7; i++)
				{
					 consoleLines[i] = consoleLines[i + 1];
					 sajt.Inlines.Add(consoleLines[i]);
				}
				consoleLines[7] = text;
				sajt.Inlines.Add(consoleLines[7]);
		  }

		  //Menu event handler
		  private void OnClose(object sender, System.ComponentModel.CancelEventArgs e)
		  {
				programRunning = false;
		  }

		  private void cbMonitorON(object sender, RoutedEventArgs e) { Globals.isMonitoring = true; /*UpdateConsole("Monitor - ON...\n");*/ }
		  private void cbMonitorOFF(object sender, RoutedEventArgs e) { Globals.isMonitoring = false; /*UpdateConsole("Monitor - OFF...\n"); */}

		  private void cbCrosshairON(object sender, RoutedEventArgs e) { Globals.isCrosshair = true; /*UpdateConsole("Crosshair - ON...\n"); */}
		  private void cbCrosshairOFF(object sender, RoutedEventArgs e) { Globals.isCrosshair = false; /*UpdateConsole("Crosshair - OFF...\n");*/ }

		  private void btExit(object sender, RoutedEventArgs e) {
				UpdateConsole("Exit...\n");
				Close();
		  }
		  
		  #endregion


		  #region OVERLAY WINDOW
		  //Main Thread
		  static void OverlayMain()
		  {
				var wndw = new OverlayWindow();
				wndw.Initialize();
				wndw.Run();

				//RunSystemTray();
				
				while (programRunning)
				{
					 
					 Thread.Sleep(500);
				}

				wndw.Stop();
		  }

		  private static void RunSystemTray()
		  {
				//Setting up context menu for system tray
				var menuItem = new Forms.MenuItem();
				menuItem.Index = 0;
				menuItem.Text = "E&xit";
				menuItem.Click += new EventHandler(TrayExitButton);

				var contextMenu = new Forms.ContextMenu();
				contextMenu.MenuItems.AddRange(new Forms.MenuItem[] { menuItem });

				//Other dependancies
				var icon = new Forms.NotifyIcon();

				icon.Icon = new Icon("./Resources/icon.ico");
				icon.Visible = true;
				icon.BalloonTipText = "Upcoming feature";
				icon.BalloonTipTitle = "Tray Icon";
				icon.BalloonTipIcon = Forms.ToolTipIcon.Info;
				icon.ShowBalloonTip(1000);
				icon.ContextMenu = contextMenu;

				icon.DoubleClick += new EventHandler(TrayDoubleClick);
		  }

		  private static void TrayExitButton(object Sender, EventArgs e)
		  {
				Console.WriteLine("Close... bla bla");
		  }
		  private static void TrayDoubleClick(object Sender, EventArgs e)
		  {
				Console.WriteLine("Ayy, leave me!");
		  }
		  #endregion
		  
	 }
}
