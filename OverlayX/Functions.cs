﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace OverlayX
{
	 public class Functions
	 {
        private static bool threadsEnabled { get; set; }

        private static int screenWidth { get; set; }
		  private static int screenHeight { get; set; }
        private static int mouseX { get; set; }
        private static int mouseY { get; set; }
        private static float cpuUtilization { get; set; }
        private static float ramUtilization { get; set; }
        private static float systemMemory { get; set; }

        Thread screenSizeObserver = new Thread(ScreenSizeUpdate);
        Thread systemObserver = new Thread(ReadSystemValues);
        Thread mainThread = new Thread(MasterThread);

        public Functions()
		  {
            //INITIALIZING VARIABLES
            SetScreenVariables();
            threadsEnabled = true;
            systemMemory = 16 * 1024;



            //LAUNCHING HANDLER THREADS
            LaunchThreads();

		  }

        ~Functions()
        {
            if(threadsEnabled) threadsEnabled = false;

        }

        private void LaunchThreads()
        {
            screenSizeObserver.Start();
            systemObserver.Start();
            mainThread.Start();
        }

        public static void MasterThread() //Controls the whole program
        {
            Console.WriteLine("Main thread started!");
            while (threadsEnabled)
            {
                ReadMousePosition();

					 
                Thread.Sleep(50);
            }
        }

        public void EXIT()
        {
            threadsEnabled = false;
        }

        /* SCREEN */
        public int GetScreenWidth()
        {
            return screenWidth;
        }
        public int GetScreenHeight()
        {
            return screenHeight;
        }
        private void SetScreenVariables()
        {
            screenWidth = Convert.ToInt32(SystemParameters.PrimaryScreenWidth);
            screenHeight = Convert.ToInt32(SystemParameters.PrimaryScreenHeight);
        }
        private static void ScreenSizeUpdate() //Detects if the screensize has changed
        {
            Console.WriteLine("Screen Size: " + screenWidth + " X " + screenHeight);
            while (threadsEnabled)
            {
                if (screenWidth != Convert.ToInt32(SystemParameters.PrimaryScreenWidth))
                    screenWidth = Convert.ToInt32(SystemParameters.PrimaryScreenWidth);

                if (screenHeight != Convert.ToInt32(SystemParameters.PrimaryScreenHeight))
                    screenHeight = Convert.ToInt32(SystemParameters.PrimaryScreenHeight);

                Thread.Sleep(3000);
            }
        }

        /* MOUSE AND KEY CAPTURE */
        public int[] GetMousePosition()
        {
            int[] packet = new int[2] { mouseX, mouseY };
            return packet;
        }
        private static void ReadMousePosition()
        {
            mouseX = Control.MousePosition.X;
            mouseY = Control.MousePosition.Y;
        }
        private void ReadKeyPress()
        {
            Console.WriteLine("placeholder");
        }

        /* READ SYSTEM VALUES */
        public float GetCpuUtilization()
        {
            return cpuUtilization;
        }
        public float GetRamUtilization()
        {
            return ramUtilization;
        }
        public float GetAvailableMemory()
        {
            return systemMemory;
        }
        private static void ReadSystemValues()
        {
            PerformanceCounter cpuCounter;
            PerformanceCounter ramCounter;

            cpuCounter = new PerformanceCounter();
            ramCounter = new PerformanceCounter("Memory", "Available MBytes");

            cpuCounter.CategoryName = "Processor";
            cpuCounter.CounterName = "% Processor Time";
            cpuCounter.InstanceName = "_Total";

            while (threadsEnabled)
            {
                cpuUtilization = cpuCounter.NextValue();
                ramUtilization = systemMemory - ramCounter.NextValue();


                Thread.Sleep(200);
            }
        }


    }
}
