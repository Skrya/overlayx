﻿using System;

namespace OverlayX
{
	 public class Globals
	 {
		  //System Specs
		  public static int screenWidth { get; }
		  public static int screenHeight { get; }

		  public static int systemMemory { get; }
		  public static int cpuCoreCount { get; }

		  //Overlay Elements
		  public static bool isCrosshair { get; set; }
		  public static bool isMonitoring { get; set; }


		  public void updateScreen()
		  {

		  }

		  public void updateSystemSpec()
		  {

		  }
		  
	 }
}
